<?php

/**
 * @file
 * Adds TightURL-like functionality to your site
 * Some code 
 */

/**
 * Implementation of hook_help().
 */
function knurl_help($section) {
  switch ($section) {
    case 'admin/help#knurl':
      return '<p>'. t('Knurl module provides functionality like TightURL service.') .'</p>';
  }
}

/**
 * Implementation of hook_perm().
 */
function knurl_perm() {
  return array('create knurl', 'access knurl', 'remove knurl');
}

/**
 * Implementation of hook_access().
 */
function knurl_access($op, $node) {
  if ($op == 'create') return user_access('create knurl');
  else if ($op == 'remove') return user_access('remove knurl');
  else return user_access('access knurl');
}

/**
 * Implementation of hook_menu().
 */
function knurl_menu($may_cache) {
  $items = array();
  if($may_cache) {
    $items[] = array(
      'path'               => 'knurl',
      'title'              => t('Create a new URL shortcut'),
      'access'             => user_access('create knurl'),
      'callback'           => 'drupal_get_form',
      'callback arguments' => array ( 'knurl_add_form' )
    );
    $items[] = array(
      'path'     => 'knurl/show',
      'title'    => t('Show URL'),
      'access'   => user_access('access knurl'),
      'callback' => 'knurl_show',
      'type'     => MENU_CALLBACK
    );
    $items[] = array(
      'path'     => 'knurl/remove',
      'title'    => t('Remove URL'),
      'access'   => user_access('remove knurl'),
      'callback' => 'knurl_remove',
      'type'     => MENU_CALLBACK
    );
    $items[] = array(
      'path'     => 'url',
      'title'    => t('redirect'),
      'access'   => user_access('access knurl'),
      'callback' => 'knurl_redirect',
      'type'     => MENU_CALLBACK
    );   
  }
  return $items;
}

/**
 * Responds to /knurl/show to list all URL's
 * and /knurl/show/${url} for one
 */
function knurl_show() {
  $output = '<p>';
  $url_limit = 75;
  $args = func_get_args();
  if( sizeof($args) == 1 ) {
    $output .= l('show all', 'knurl/show'); 
    $res = db_query("select * from {knurl} where short_url='%s'", end($args));
  } else {
    $output .= '&nbsp;';
    $res = pager_query("select * from {knurl} order by tid desc", 50);
  }
  if(db_num_rows($res) == 0) return drupal_not_found();
  $output .= '<table style="margin:0px;padding:0px;"><thead><tr>';
  $output .= '<th style="width:80%">Target URL</th>';
  $output .= '<th style="width:20%">knurl</th>';
  //$output .= '<th style="width:20%">Action</th></tr></thead>';
  while ($node = db_fetch_object($res)) {
    $output .= '<tr><td>' . wordwrap($node->link, $url_limit, "<BR>", true) . '</td><td>';
    $output .= l(_get_server_url() . '/url/' . $node->short_url, 'url/' . $node->short_url);
    $output .= '</td>';//<td>' . l("delete", 'knurl/remove/' . $node->short_url) . '</td></tr>';
    // why do we need to delete anyway?  commented out by Sam Placette, 9-26-07
  }
  $output .= '</table>';
  $output .= theme("pager", array( 2=>3 ), 50);
  return $output; 
}

/**
 * Responds to /knurl/delete/${url}
 *
 * It handles knurl removal
 */
function knurl_remove() {
  $short_url = end($args= func_get_args());
  if (_knurl_is_unique_url($short_url)) {
    drupal_set_message(t(sprintf('URL <b>%s</b> was not found', $short_url)));
    return drupal_goto("knurl/show/");
  }
  else {
    db_query("DELETE FROM {knurl} WHERE short_url='%s'", $short_url);
    drupal_set_message(t( sprintf('URL <b>%s</b> was removed', $short_url) ));
    return drupal_goto("knurl/show/");
  }
}

/**
 * Responds to /url/${url}
 *
 * Redirects the user to the given URL
 */
function knurl_redirect() {
  $short_url = end($args = func_get_args());
  $res = db_fetch_object(db_query("select * from {knurl} where short_url='%s'", $short_url));
  if(!$res) return drupal_not_found();
  else return drupal_set_header("Location: ".$res->link);
  // else return drupal_set_html_head('<meta http-equiv="refresh" content="0;url='.$res->link.'"/>');
}

// form for adding a new knurl
function knurl_add_form() {
  drupal_set_message( t(sprintf('Shorten long URLs by using a URL shortcut instead. Just enter the original URL (including http(s)://) and click Submit, and a new URL shortcut will be generated.')) );
  drupal_set_message( t(sprintf('Now you can create knurls even easier than before.  Just drag this <A HREF="javascript:void(location.href=\''._get_server_url().'/knurl?url=\'+location.href)">%s knurl</A> link to your toolbar. At the click of a button, a URL shortcut will be generated for the page you are currently at.', variable_get('site_name', 'Drupal'))));
  $form['url'] = array( '#type' => 'textarea',
    '#title' => t('Enter a fully qualified URL'),
    '#default_value' => $_GET['url'],
    '#required' => TRUE
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  if (isset($_GET['url']) && _knurl_is_valid_url($_GET['url'])) { // automagically submit if valid url is included in URI
    $form_values['url'] = $_GET['url'];
    knurl_add_form_submit( 0, $form_values);
  }
  return $form;
}

// callback for validating the form
function knurl_add_form_validate($form_id, $form_values) {
  if(!_knurl_is_valid_url($form_values['url']) ) {
    form_set_error('', t('Invalid URL'));
  }
}

// callback executed on submit
function knurl_add_form_submit( $form_id, $form_values ) {
  global $user;
  $url_limit = 75;
  $target_url = $form_values['url'];
  // generate a unique URL
  do {
    $short_url = _knurl_random_url();
  } while( !_knurl_is_unique_url( $short_url ) );
  db_query("insert into {knurl} (uid, short_url, link) values (%d, '%s', '%s')", $user->uid, $short_url, $target_url);

  if(strlen($target_url) > $url_limit) {
    $target_url = "<BR>".wordwrap($form_values['url'], $url_limit, "<BR>", true);
  }
  if($_SERVER['HTTPS']) { 
    $protocol = "https://";
  } else $protocol = "http://";
  drupal_get_messages();
  drupal_set_message( t(sprintf('The URL shortcut was successfully created. <A HREF="/url/%s">%s/url/%s</A> now links to %s.<P>You can use the URL shortcut anywhere you would use the original URL.', $short_url, _get_server_url(), $short_url, $target_url)) );
  return drupal_goto( sprintf('knurl/show/%s', $short_url) );
}

/**
 * Generates a random url
 *
 * based on horde, Auth::getRandomPassword
 */
function _knurl_random_url() {
  $vowels    = 'aeiouy';
  $constants = 'bcdfghjklmnpqrstvwxz';
  $numbers   = '0123456789';
  $chars[0] = substr($constants, mt_rand(0, strlen($constants) - 1), 1);
  $chars[1] = substr($vowels, mt_rand(0, strlen($vowels) - 1), 1);
  $chars[2] = substr($constants, mt_rand(0, strlen($constants) - 1), 1);
  $chars[3] = substr($vowels, mt_rand(0, strlen($vowels) - 1), 1);
  $chars[4] = substr($constants, mt_rand(0, strlen($constants) - 1), 1);
  $chars[5] = substr($numbers, mt_rand(0, strlen($numbers) - 1), 1);
  $chars[6] = substr($numbers, mt_rand(0, strlen($numbers) - 1), 1);
  shuffle($chars);
  return implode( '', $chars );
}

/**
 * Checks if the given URL is valid
 */
function _knurl_is_unique_url( $url ) {
  $result = db_num_rows( db_query("select short_url from {knurl} where short_url='%s'", $url) );
  return $result == 0;
}

/**
 * Check if the given URL is valid
 *
 * based on http://tighturl.com/sourcecode/tighturl.php
 */
function _knurl_is_valid_url( $url ) {
  $validurlpattern  = "/^(http|https|ftp|sftp)\:\/\/([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&%\$\-]+)*@)";
  $validurlpattern .= "*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])";
  $validurlpattern .= "\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)";
  $validurlpattern .= "\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)";
  $validurlpattern .= "\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])";
  $validurlpattern .= "|((([0-9A-F]{1,4}(((:[0-9A-F]{1,4}){5}::[0-9A-F]{1,4})|((:[0-9A-F]{1,4}){4}";
  $validurlpattern .= "::[0-9A-F]{1,4}(:[0-9A-F]{1,4}){0,1})|((:[0-9A-F]{1,4}){3}::[0-9A-F]{1,4}";
  $validurlpattern .= "(:[0-9A-F]{1,4}){0,2})|((:[0-9A-F]{1,4}){2}::[0-9A-F]{1,4}(:[0-9A-F]{1,4})";
  $validurlpattern .= "{0,3})|(:[0-9A-F]{1,4}::[0-9A-F]{1,4}(:[0-9A-F]{1,4}){0,4})|(::[0-9A-F]{1,4}";
  $validurlpattern .= "(:[0-9A-F]{1,4}){0,5})|(:[0-9A-F]{1,4}){7}))|(::[0-9A-F]{1,4}(:[0-9A-F]{1,4}";
  $validurlpattern .= "){0,6}))|::)|((([0-9A-F]{1,4}(((:[0-9A-F]{1,4}){3}::([0-9A-F]{1,4}){1})";
  $validurlpattern .= "|((:[0-9A-F]{1,4}){2}::[0-9A-F]{1,4}(:[0-9A-F]{1,4}){0,1})|((:[0-9A-F]{1,4})";
  $validurlpattern .= "{1}::[0-9A-F]{1,4}(:[0-9A-F]{1,4}){0,2})|(::[0-9A-F]{1,4}(:[0-9A-F]{1,4}";
  $validurlpattern .= "){0,3})|((:[0-9A-F]{1,4}){0,5})))|([:]{2}[0-9A-F]{1,4}(:[0-9A-F]{1,4}){0,4}))";
  $validurlpattern .= ":|::)((25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{0,2})\.){3}(25[0-5]|2[0-4][0-9]|";
  $validurlpattern .= "[0-1]?[0-9]{0,2})";
  $validurlpattern .= "|localhost|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org";
  $validurlpattern .= "|biz|arpa|info|name|pro|aero|coop|museum|us|ca|uk|jp|ru|ua|fr|gr|de|dk|tr|ie";
  $validurlpattern .= "|it|au|br|cc|ch|cn|es|fi|gr|hk|hu|il|in|kr|mx|my|nl|no|nu|nz|ph|pl";
  $validurlpattern .= "|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm";
  $validurlpattern .= "|bn|bo|bs|bt|bv|bw|by|bz|cf|cd|cg|ch|ci|ck|cl|cm|co|cr|cs|cu|cv|cx|cy|cz|dj";
  $validurlpattern .= "|dm|do|dz|ec|ee|eg|eh|er|et|fj|fk|fm|fo|fx|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gp";
  $validurlpattern .= "|gq|gs|gt|gu|gw|gy|hm|hn|hr|ht|id|io|iq|ir|is|jm|jo|ke|kg|kh|ki|km|kn|kp|kw";
  $validurlpattern .= "|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq";
  $validurlpattern .= "|mr|ms|mt|mu|mv|mw|mz|na|nc|ne|nf|ng|ni|np|nr|nt|om|pa|pe|pf|pg|pk|pm|pn|pr";
  $validurlpattern .= "|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su";
  $validurlpattern .= "|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|um|uy|uz|va";
  $validurlpattern .= "|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zr|zw|eu";
  $validurlpattern .= "|[a-zA-Z]{2}))(\:[0-9]+)*(\/($|[a-zA-Z0-9\.\:\,\?\'\\\+&%\$#\=~_\-\s@]+))*$/";
  return preg_match($validurlpattern, $url);
}

// return a link to the server, including protocol
function _get_server_url() {
  if($_SERVER['HTTPS']) {
    $protocol = "https://";
  } else $protocol = "http://";
  return $protocol.$_SERVER['HTTP_HOST'];
}
